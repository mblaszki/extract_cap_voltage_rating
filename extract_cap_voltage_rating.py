#!/usr/bin/env python3
# mypy: ignore-errors

import argparse
import re
import codecs
from typing import Dict

import pandas
import tabulate
import spydrnet as sdn

# Some netlipst pre-processing (sanitizing) is needed:
# 1. remove double quotes from description strings, they throw off the
#    tokenizer
# 2. Make stuff like
#      Instance (rename R44_MoniMod_I2C_Translation___SCL1 "R44_MoniMod I2C Translation - SCL1")
#    look like
#      Instance (rename R44_MoniMod_I2C_Translation___SCL1 "R44_MoniMod_I2C_Translation___SCL1")

# parse custom net voltage declaration file.

def run_extraction(in_file: str, net_voltage_file: str = "", net_ignore_file: str = "",
                   export_spreadsheet: bool = False, sanitize: bool = False, count_ic_pins: bool = False):

    def parse_net_voltage_file(filename) -> Dict[str, float]:
        found_custom_rails = {}
        with open(filename, 'r', encoding="utf-8") as f:
            lines = [line.strip() for line in f]
        for l in lines:
            ls = [item.strip() for item in l.split(':')]
            assert len(ls) == 2, 'The line format is: "NET_NAME: voltage"'
            found_custom_rails[ls[0]] = float(ls[1])
        return found_custom_rails

    custom_rails = {}
    if net_voltage_file:
        print(f"Parsing net voltage input file {net_voltage_file}...")
        custom_rails = parse_net_voltage_file(net_voltage_file)

    # parse net ignore declaration file.
    ignore_nets = []

    def net_ignored(nn):
        for net in ignore_nets:
            if re.match(net, nn):
                print(f"net {nn} matches {net}, ignoring.")
                return True
        return False

    def parse_net_ignore_file(filename):
        found_ignore_nets = []
        with open(filename, 'r') as f:
            found_ignore_nets = [line.strip() for line in f]
        return found_ignore_nets

    if net_ignore_file:
        print(f"Parsing net ignore input file {net_ignore_file}...")
        ignore_nets = parse_net_ignore_file(net_ignore_file)

    def sanitize_input():
        print(f"Sanitizing input file {in_file}...")
        replacements = {}
        outlines = []
        cell_names = []
        inst_sub_dot = []
        cell_id_iter = 0
        with codecs.open(in_file, 'r', encoding='utf-8', errors='ignore') as f:
            for line in f:
                if outlines and line == outlines[-1]:
                    print(f"same line repeated, skipping: {line}")
                    continue
                line = line.replace("�", "") # removing a replacement character which causes the script to crash
                line = line.replace("rename _", "rename ")
                match = re.match(r'.*Instance \(rename (\S+) "(.+)".*', line)
                if match and match[2].find(' ') > -1:
                    line = re.sub(r'(.*)rename (\S+) ".+?"(.*)', r'\1rename \2 "\2"\3', line)
                    print('replacing "{}" with "{}" from now on'.format(match[2], match[1]))
                    replacements[match[2]] = match[1]
                for repl in replacements.keys():
                    if line.find(repl) > -1:
                        print('replacing "{}" with "{}"'.format(repl, replacements[repl]))
                        line = re.sub(r'(.*){}(.*)'.format(repl), r'\1'+replacements[repl]+r'\2', line)
                match = re.match(r'(.*)""(.*)""(.*)', line)
                if match:
                    print('found problematic case with double quotes in strings: {}'.format(line))
                    line = match[1] + '"\'' + match[2].replace('"', '\'') + '\'"' + match[3]
                    print('replacing with {}'.format(line))
                match = re.match(r'(.*)cell \(rename (&*\w+) "(.+)"\)', line)
                if match:
                    if match[2] in cell_names:
                        newname = match[2] + str(cell_id_iter)
                        line = match[1] + 'cell (rename ' + newname + \
                        ' "' + match[3] + str(cell_id_iter) + '")'
                        cell_id_iter += 1
                        print('found duplicate cell definition, renaming to {}'.format(newname))
                    cell_names += [match[2]]
                match = re.match(r'(.*)cell (\w+)', line)
                if match:
                    if match[2] in cell_names:
                        newname = match[2] + str(cell_id_iter)
                        line = match[1] + 'cell ' + newname
                        cell_id_iter += 1
                        print('found duplicate cell definition, renaming to {}'.format(newname))
                    cell_names += [match[2]]
                match = re.match(r'.*Instance \(rename \w+ "([.\w]+)"\).*', line)
                if match and match[1].find('.') > -1:
                    print('dot in {} to be replaced with underscore'.format(match[1]))
                    inst_sub_dot += [match[1]]
                for a in inst_sub_dot:
                    if a in line:
                        oldline = line
                        line = line.replace('.', '_')
                        print('replaced {} with {}'.format(oldline, line))
                if line.count('"') % 2:
                    match = re.match(r'.*(\(.\d+(")\)).*', line)
                    if match:
                        print('found problematic string {}'.format(match[1]))
                        line = re.sub(r'(.*\(.\d+)"(\).*)', r"\1''\2", line)
                        print('Removing the double quotes in it, line became {}'.format(line.strip()))
                    match = re.match(r'.*(".*\d+"\ .*").*', line)
                    if match:
                        print('found problematic string {}'.format(match[1]))
                        line = re.sub(r'(.*".*\d+)"(\ .*".*)', r"\1''\2", line)
                        print('Removing the double quotes in it, line became {}'.format(line.strip()))
                outlines += [line]
        with open(in_file, 'w') as f:
            f.writelines(outlines)
    if sanitize:
        sanitize_input()

    # This parses two libraries: the first one is the pretty useless
    # COMPONENT_LIB that defines components in the EDIF. The other one is
    # the design library, that's what we want. The one and only item of
    # its definitions list is the design top-level, it seems,
    # DIOT_sb_igl_top_SchDoc.

    netlist = sdn.parse(in_file)

    top_level = netlist.libraries[1].definitions[0]
    insts = top_level.children

    caps = [a for a in insts if re.match("C[0-9]+", a.name)]
    diodes = [diode for diode in insts if re.match("D[0-9]+", diode.name)]
    inductors = [a for a in insts if re.match("L[0-9]+", a.name)]
    resistors = [a for a in insts if re.match("R[0-9]+", a.name)]
    net_ties = [a for a in insts if re.match("NT[0-9]+", a.name)]
    tied_nets = {}
    ics = [a for a in insts if (re.match("IC[0-9]+", a.name) or re.match("U[0-9]+", a.name))]

    def get_voltage(nn):
        """This method attempts to discover the voltage from a net name.
        """

        # first try out user-defined nets in the net_voltage_file
        for custom in custom_rails:
            if re.match(custom, nn):
                return custom_rails[custom]

        # is it a ground like GND, AGND, GND_ISO_BLA?
        match_gnd = re.match(".?GND.*", nn)
        if match_gnd:
            return 0

        # is it a 0V?
        match_zero = re.match("^0V.*", nn)
        if match_zero:
            return 0

        # does it end in VSS(A)?
        match_vssa = re.match(".*VSSA?", nn)
        if match_vssa:
            print('Net {} taken to be GND'.format(nn))
            return 0

        # then try names like P3V3, P12V, +1V2_DDR4, etc
        match_pavb = re.match(r"[a-zA-Z_]*([P+MN])(\d+)V?(\d+)?.*", nn)
        if match_pavb:
            if match_pavb[3] is None:
                res = float(match_pavb[2])
                if match_pavb[1] in 'P+':
                    return res
                return -res
            else:
                res = float(match_pavb[2]) + float('0.' + match_pavb[3])
                if match_pavb[1] in 'P+':
                    return res
                return -res

        # then try names like 3V3P, 5V0N, etc
        match_avbp = re.match(r"[a-zA-Z_]*(\d+)[VP](\d+)([PMN])?.*", nn)
        if match_avbp:
            if match_avbp[2] is None:
                res = float(match_avbp[1])
                if match_avbp[3] is None or match_avbp[3] == 'P':
                    return res
                return -res
            else:
                res = float(match_avbp[1]) + float('0.' + match_avbp[2])
                if match_avbp[3] is None or match_avbp[3] == 'P':
                    return res
                return -res

        # then names like +5V, -15V, +13.1V_BLABLA, etc
        match_pmav = re.match(r"([+-][0-9\.]+)V?.*", nn)
        if match_pmav:
            return float(match_pmav[1])

        # then check the tied net list
        if nn in tied_nets:
            return tied_nets[nn]

        # no luck, return None
        return None

    def get_prop(comp, name):
        prop_list = [a['value'] for a in comp.data['EDIF.properties'] if a['identifier'] == name]
        if len(prop_list) == 1:
            return prop_list[0]
        return '-'

    def parse_voltage_rating(rr: str) -> float:
        if rr == '-':
            print(f"WARNING: Found undefined voltage capacitor {name}, setting its rating to 0")
            rr = '0V'
        matching_groups = re.match(r"([0-9]+)(\.|kV|V)([0-9]*)", rr)
        result = float(matching_groups[1]+"."+matching_groups[3])
        if matching_groups[2] == "kV" :
            return 1000*result
        return result

    def get_net_tie_dict(comps, tied_net_dict, check=None):
        for a in comps:
            name = a.name
            if None in [p.wire for p in a.pins]:
                print(f"WARNING: possible floating pin in {name}")
                continue
            nets = [p.wire.cable.name for p in a.pins]
            if len(nets) != 2:
                continue
            if net_ignored(nets[0]) or net_ignored(nets[1]):
                continue
            v_n0 = get_voltage(nets[0])
            v_n1 = get_voltage(nets[1])
            # If we only recognized one of the two voltages
            if ((v_n0 is None) + (v_n1 is None)) == 1:
                # run the optional check
                if check is not None:
                    if not check(a):
                        continue
                if v_n0 is None:
                    tied_net_dict[nets[0]] = float(v_n1)
                else:
                    tied_net_dict[nets[1]] = float(v_n0)

    def res_check(a):
        val_str = get_prop(a, 'Value')
        if ('0R0' in val_str) and (val_str == '0'):
            return True
        return False

    get_net_tie_dict(inductors, tied_nets, None)
    get_net_tie_dict(net_ties, tied_nets, None)
    get_net_tie_dict(resistors, tied_nets, res_check)

    known_caps = {'name': [],
                'value': [],
                'voltage': [],
                'rating': [],
                'pct': [],
                'absdiff': [],
                'pin1': [],
                'pin2': [],
                'tc': [],
                'footprint': [],
                'desc': []}

    unknown_caps = {'name': [],
                    'value': [],
                    'rating': [],
                    'pin1': [],
                    'pin2': [],
                    'tc': [],
                    'footprint': [],
                    'desc': []}

    ignored_caps = {'name': [],
                    'value': [],
                    'rating': [],
                    'pin1': [],
                    'pin2': [],
                    'tc': [],
                    'footprint': [],
                    'desc': []}

    for c in [*caps, *diodes]:
        name = c.name
        if None in [p.wire for p in c.pins]:
            print('WARNING: possible floating pin in {}'.format(name))
            continue
        nets = [p.wire.cable.name for p in c.pins]
        if len(nets) != 2:
            continue

        rating_s = get_prop(c, 'Voltage')
        rating = parse_voltage_rating(rating_s)

        tc = get_prop(c, 'TC')
        desc = get_prop(c, 'Part_Description')
        value = get_prop(c, 'Value')
        library_ref = get_prop(c, 'Library_Reference')
        fp_match = re.match('(.*?)_.*', library_ref)
        if fp_match:
            footprint = fp_match[1]
        else:
            footprint = '-'

        if net_ignored(nets[0]) or net_ignored(nets[1]):
            ignored_caps['name'] += [name]
            ignored_caps['value'] += [value]
            ignored_caps['rating'] += [rating]
            ignored_caps['pin1'] += [nets[0]]
            ignored_caps['pin2'] += [nets[1]]
            ignored_caps['tc'] += [tc]
            ignored_caps['footprint'] += [footprint]
            ignored_caps['desc'] += [desc]
            continue

        v1 = get_voltage(nets[0])
        v2 = get_voltage(nets[1])
        if (v1 is not None) and (v2 is not None):
            rail_v = abs(v1 - v2)
            if rating != 0:
                pct = rail_v / float(rating)
                absdiff = float(rating) - rail_v
            else:
                pct = 100
                absdiff = 0
            known_caps['name'] += [name]
            known_caps['value'] += [value]
            known_caps['voltage'] += [rail_v]
            known_caps['rating'] += [rating]
            known_caps['pct'] += [pct]
            known_caps['absdiff'] += [absdiff]
            known_caps['pin1'] += [nets[0]]
            known_caps['pin2'] += [nets[1]]
            known_caps['tc'] += [tc]
            known_caps['footprint'] += [footprint]
            known_caps['desc'] += [desc]
        else:
            unknown_caps['name'] += [name]
            unknown_caps['value'] += [value]
            unknown_caps['rating'] += [rating]
            unknown_caps['pin1'] += [nets[0]]
            unknown_caps['pin2'] += [nets[1]]
            unknown_caps['tc'] += [tc]
            unknown_caps['footprint'] += [footprint]
            unknown_caps['desc'] += [desc]

    known_df = pandas.DataFrame(known_caps)
    unknown_df = pandas.DataFrame(unknown_caps)
    ignored_df = pandas.DataFrame(ignored_caps)

    known_df = known_df.sort_values('pct', ascending=False, ignore_index=True)
    unknown_df = unknown_df.sort_values('rating', ascending=True, ignore_index=True)
    ignored_df = ignored_df.sort_values('name', ascending=True, ignore_index=True)

    print('Succeeded to extract the voltage applied to the following capacitors:')
    print(tabulate.tabulate(known_df, headers='keys'))
    print()

    print('I don\'t know the voltage applied to the following capacitors:')
    print(tabulate.tabulate(unknown_df, headers='keys'))

    if export_spreadsheet:
        spreadsheet_filename = in_file[:-3] + 'csv'
        print(f"Exporting spreadsheet as {spreadsheet_filename}")
        known_df.to_csv(spreadsheet_filename)
        unknown_df.to_csv(in_file[:-4] + '_unknown.csv')
        ignored_df.to_csv(in_file[:-4] + '_ignored.csv')

    if count_ic_pins:
        print('Counting total IC pins...')
        total_ic_pins = 0
        ic_pin_hist = {}
        for ic in ics:
            name = ic.name
            if None in [p.wire for p in ic.pins]:
                print('WARNING: possible floating pin in {}'.format(name))
            ic_pins = len(ic.pins)
            print('{} has {} pins'.format(name, ic_pins))
            total_ic_pins += ic_pins
            if ic_pins in ic_pin_hist:
                ic_pin_hist[ic_pins] += 1
            else:
                ic_pin_hist[ic_pins] = 1
        for a in ic_pin_hist.keys():
            if ic_pin_hist[a] == 1:
                there_is_there_are_str = 'There is'
                ic_ics_str = 'IC'
            else:
                there_is_there_are_str = 'There are'
                ic_ics_str = 'ICs'
            print('{} {} {} with {} pins'.format(there_is_there_are_str, ic_pin_hist[a], ic_ics_str, a))
        print('In total, there are {} IC pins.'.format(total_ic_pins))
        print(ic_pin_hist)
    return known_df

if __name__ == "__main__":
    def config_parser():
        parser = argparse.ArgumentParser(description='Extract capacitors from an\
        Altium-generated EDIF netlist to check if the voltage ratings are adhered to.')
        parser.add_argument('in_file', type=str, help='Input netlist filename')
        parser.add_argument('-n', '--net_voltage_file', type=str, help='Net voltage declaration file (one voltage / line," \
                            "e.g. "VREF1: 2.5")')
        parser.add_argument('-i', '--net_ignore_file', type=str, help='Net ignore file (one regexp / line, e.g." \
                            ".*DP_C2M.*")')
        parser.add_argument('-e', '--export_spreadsheet', action='store_true', help='Export the results as an .ods" \
                            "spreadsheet')
        parser.add_argument('-s', '--sanitize', action='store_true', help='Sanitize netlist before running (modifies the" \
                            "input file)')
        parser.add_argument('-c', '--count_ic_pins', action='store_true', help='Report a pin count for the board\'s ICs')
        return parser.parse_args()

    args = config_parser()
    run_extraction(args.in_file, args.net_voltage_file, args.net_ignore_file, args.export_spreadsheet, args.sanitize, 
                   args.count_ic_pins)